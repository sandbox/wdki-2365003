/**
 * @file list_with_other.js
 * 
 */

(function ($) {

  Drupal.behaviors.listOther = {
    attach: function(context, settings) {

      // Select
      $('select.list-other-list-js')
      .change(function() {
        var parent = $(this).parents()[1];
        var show_or_hide = $(this).find('[value="other_value"]').is(':selected');
        $(parent).find('.list-other-other-js').parent().toggle(show_or_hide);
      })
      .once('list-other-select-init').change();

      // Checboxes
      $('input[value=other_value].list-other-list-js')
      .click(function() {
        var parent = $(this).parents()[3];
        var show_or_hide = $(this).is(':checked');
        $(parent).find('.list-other-other-js').parent().toggle(show_or_hide);
      })
      .once('list-other-select-init', function() {
        var parent = $(this).parents()[3];
        var show_or_hide = $(this).is(':checked');
        $(parent).find('.list-other-other-js').parent().toggle(show_or_hide);
      });

    }
  };

})(jQuery);
