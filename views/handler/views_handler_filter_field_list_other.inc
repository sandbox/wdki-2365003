<?php

/**
 * @file
 * Definition of views_handler_filter_field_list.
 */

/**
 * Filter handler which uses list-fields as options.
 *
 * @ingroup views_filter_handlers
 */
class views_handler_filter_field_list_other extends views_handler_filter_many_to_one {

  function init(&$view, &$options) {
    parent::init($view, $options);
    // Migrate the settings from the old filter_in_operator values to filter_many_to_one.
    if ($this->options['operator'] == 'in') {
      $this->options['operator'] = 'or';
    }
    if ($this->options['operator'] == 'not in') {
      $this->options['operator'] = 'not';
    }
    $this->operator = $this->options['operator'];
  }


  function get_value_options() {
    $field = field_info_field($this->definition['field_name']);
    // @todo find a way to get other value label fotm field instance here or in list_other_field_views_data_views_data_alter()
    $this->value_options = _list_other_allowed_values($field) + array('other_value' => t('Other'));
  }
}
