<?php
/**
 * @file
 * Views integration
 * 
 */

/**
 * Implementation of hook_field_views_data_views_data_alter().
 */

function list_other_field_views_data_views_data_alter(&$data, $field) {
  $current_table = _field_sql_storage_tablename($field);
  $revision_table = _field_sql_storage_revision_tablename($field);

  $tables = array();
  $tables[FIELD_LOAD_CURRENT] = $current_table;
  if(isset($data[$revision_table])) {
    $tables[FIELD_LOAD_REVISION] = $revision_table;
  }

  // Alter handler only for the value column
  $column = 'value';
  foreach ($tables as $type => $table) {
    $column_real_name = $field['storage']['details']['sql'][$type][$table][$column];
    if(array_key_exists($column_real_name, $data[$table])) {
      $data[$table][$column_real_name]['argument']['handler'] = 'views_handler_argument_field_list_other';
      $data[$table][$column_real_name]['filter']['handler'] = 'views_handler_filter_field_list_other';
    }
  }
}
